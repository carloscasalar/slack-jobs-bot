const axios = require('axios');
const cheerio = require('cheerio');
const { 
  parseDate,
  postedYesterdayJob, 
  frontendJob, 
  backendJob, 
  logJob, 
  and, 
  or,
} = require('./lib/utils'); 

// Giving a DOM .job-item elment returns a job object
// scrapeJob : Html -> Job
const scrapeJob = $job => ({
  title: $job.find('.job-item__title').text(),
  title_link: $job.find('.job-title').attr('href'),
  date: parseDate($job.find('.job-item__date').text()),
  text: $job.find('.job-item__excerpt').text(),
});

const getJobs = () => 
  axios
    .get('https://www.domestika.org/es/jobs/where/remote')
    .then(response => {
      const $ = cheerio.load(response.data);

      const $jobs = $('.job-item');

      const jobs = 
        $jobs
        .map((i, el) => scrapeJob($(el)))
        .get();

      return jobs;
    });

// formatSlackMessage : Job -> SlackMessage
const formatSlackMessage = ({title, text, title_link}) => ({
  text: 'New project found!',
  attachments: [{
    title,
    text,
    title_link,
  }],
});

// send the message to the slack webhook
const sendBotMessage = message => 
  axios
    .post(process.env.SLACK_WEBHOOK, message)
    .catch(console.log);

module.exports.scrape = () => {
  console.log(`Executing task @ ${new Date()}`);

  getJobs().then(jobs =>
    jobs
    .filter(and(postedYesterdayJob, or(frontendJob, backendJob), notDesignerJob))
    .map(logJob)
    .map(formatSlackMessage)
    .forEach(sendBotMessage)
  );
};
