# Slack-jobs-bot

This is a bot who scrapes `https://www.domestika.org/es/jobs/where/remote` looking for remote jobs and then it will post the most interesting in a slack channel. The bot have a cron, and the process will run each day at 9am.

## Getting started

This bot has been developed using the [serverless](https://serverless.com/) framework.
First you will need to install its dependencies with:

```bash
npm install serverless -g
npm install 
```

Then you will need to set a environment variable `SLACK_WEBHOOK` to the webhook you got slack account. Read more about *slack webhooks* [here](https://api.slack.com/incoming-webhooks). This code takes that variable from a file called `serverless.env.yml`, you can copy the example file `example.serverless.env.yml` to that name and put inside the value of your webhook. 

Once you have set that environment variable you can try the crawler function with:

```bash
serverless invoke local -f domestika
```


## Deploy

You will need to setup your AWS account. [Get more info about AWS setup](https://serverless.com/framework/docs/providers/aws/guide/quick-start/).

After setting up AWS you just have to type:

```bash
serverless deploy
```
