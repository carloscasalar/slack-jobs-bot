const test = require('tape');
const utils = require('../../lib/utils');

const now = Date.now();

test('and', t => {
  t.plan(4);
  t.ok(utils.and(() => true)());
  t.ok(utils.and(() => true, () => true)());
  t.notOk(utils.and(() => false)());
  t.notOk(utils.and(() => true, () => false)());
});

test('or', t => {
  t.plan(4);
  t.ok(utils.or(() => true)());
  t.ok(utils.or(() => false, () => true)());
  t.notOk(utils.or(() => false)());
  t.notOk(utils.or(() => false, () => false)());
});

test('sameDay', t => {
  t.plan(3);
  t.ok(utils.sameDay(new Date(0))(new Date(0)));
  t.notOk(utils.sameDay(new Date(0))(new Date(60 * 60 * 24 * 1000)));
  t.notOk(utils.sameDay(new Date(60 * 60 * 24 * 1000))(new Date(0)));
});

test('today', t => {
  t.plan(2);
  t.ok(utils.today(new Date(Date.now())));
  t.notOk(utils.today(new Date(0)));
});

test('yesterday', t => {
  t.plan(4);
  t.ok(utils.yesterday(new Date(Date.now() - (60 * 60 * 24 * 1000))));
  t.notOk(utils.yesterday(new Date(Date.now())));
  t.notOk(utils.yesterday(new Date(Date.now() + (60 * 60 * 24 * 1000))));
  t.notOk(utils.yesterday(new Date(Date.now() - (2 * 60 * 60 * 24 * 1000))));
});
